package com.product.sender;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import com.product.feature.ScenarioFactory;

import lombok.var;

@RunWith(MockitoJUnitRunner.class)
public class ProductDeleteQueueSenderTest {
	@InjectMocks
	private ProductDeleteQueueSender productDeleteQueueSender;

	@Mock
	private RabbitTemplate rabbitTemplate;

	@Test
	public void sendMessage_WhenMessageQueue_ExpectedSucess() {
		var product = ScenarioFactory.newProduct();
		
		ReflectionTestUtils.setField(productDeleteQueueSender, "queue", "texte");
		
		productDeleteQueueSender.sendMessage(product);

		verify(rabbitTemplate, times(1)).convertAndSend("texte", product);

	}
}
